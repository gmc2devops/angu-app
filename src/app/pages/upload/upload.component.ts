import { Component, OnInit } from '@angular/core';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import * as XLSX from 'xlsx';
import * as _ from 'lodash';
import { SelectionModel } from '@angular/cdk/collections';
import { AuthService } from '@app/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogBoxComponent } from '@app/components/dialog-box/dialog-box.component';

@Component({
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  files: NgxFileDropEntry[] = [];
  bfiles = [];
  currentUser = {};
  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  displayedColumns: string[] = ['name', 'size', 'lastModifiedDate', 'details', 'select'];
  selection = new SelectionModel(true, []);

  constructor(public auth: AuthService, private http: HttpClient, private SnackBar: MatSnackBar, public dialog: MatDialog) {
    this.auth.userProfile$.subscribe(userProfile => {
      this.currentUser = userProfile;
      console.log(this.currentUser);
    });

  }

  openDialog(obj) {
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      data: obj
    });
  }

  openSnackBar(value): void {
    this.SnackBar.open(value, 'End now', {
      duration: 1000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line: typedef
  dropped(files: NgxFileDropEntry[]) {
    this.bfiles = [];
    this.files = files;
    console.log('droped');
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.bfiles.push(file);
          this.convertXLSXtoJson(file);
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  // tslint:disable-next-line: typedef
  convertXLSXtoJson(file) {
    // define
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    // reader
    reader.readAsBinaryString(file);
    // file read
    reader.onload = () => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});

      // grab bfiles
      let item = this.bfiles.find(item => item.name === file.name);
      // add json workbook
      item['workbook'] = jsonData;
    };
  }

  // tslint:disable-next-line: typedef
  fileOver(event) {
    console.log(event);
  }

  // tslint:disable-next-line: typedef
  fileLeave(event) {
    console.log(event);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  // tslint:disable-next-line: typedef
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.bfiles.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  // tslint:disable-next-line: typedef
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.bfiles.forEach(row => this.selection.select(row));
  }

  // tslint:disable-next-line: typedef
  resetFiles() {
    this.files = [];
    this.bfiles = [];
  }

  // tslint:disable-next-line: typedef
  uploadFilesToDB() {
    this.selection.selected.forEach(row => {
      const fileData = {
        lastModified: row.lastModified,
        lastModifiedDate: row.lastModifiedDate,
        name: row.name,
        size: row.size,
        type: row.type,
      };
      const document = {
        file: fileData,
        workbook: row.workbook,
        user: this.currentUser
      };
      this.http.post('http://localhost:3000/files/add', document).subscribe({
        next: data => {
          console.log('data!', data);
          this.resetFiles();
          this.openSnackBar('upload done!');
        },
        error: error => console.error('There was an error!', error)
      });
    });
  }

}
