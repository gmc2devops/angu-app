import { Component, OnInit } from '@angular/core';
import { AuthService } from '@app/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {

  panelOpenState = false;
  currentUser = {} as { email: string };
  files = [];

  constructor(public auth: AuthService, private http: HttpClient) {
    this.auth.userProfile$.subscribe(userProfile => {
      this.currentUser = userProfile;
      console.log(this.currentUser);
    });
  }

  ngOnInit(): void {
    this.loadFiles();
  }

  getColumns(obj): string[] {
    return Object.keys(obj);
  }

  sortData(sort: Sort, data) {
    console.log();
  }

  loadFiles(): void {
    this.http.post('http://localhost:3000/files', { email: this.currentUser.email }).subscribe({
      next: data => {
        this.files = _.groupBy((data as { docs: [] }).docs, 'file.name');
        console.log('data!', this.files);
      },
      error: error => console.error('There was an error!', error)
    });
  }

}
